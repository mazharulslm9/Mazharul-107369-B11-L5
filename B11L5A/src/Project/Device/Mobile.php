<?php
namespace App\Project\Device;
class Mobile {
       
    public $brand = "NOKIA ";
    public $color = " White";
    public $size = "4inch ";
    public $camera = "5MP ";
    public $weight = "4 gm";

    public function getBrand() {

        echo 'Brand of the mobile is ' . $this->brand;
    }

    public function getSize() {

        echo 'Size of the mobile is ' . $this->size;
    }

    public function getColor() {

        echo 'color of the mobile is ' . $this->color;
    }

}
