<?php

namespace App\Project\Animal;

class Cat {

    public $name = "Pussy ";
    public $color = "White";
    public $leg = 4;
    public $ear = 2;
    public $tail = 1;

    public function getName() {

        echo 'Name  of this cat  is ' . $this->name;
    }

    public function getColor() {

        echo 'Color of this cat  is ' . $this->color;
    }

    public function getLegnumber() {

        echo 'Number of leg for a cat  is ' . $this->leg;
    }

}
