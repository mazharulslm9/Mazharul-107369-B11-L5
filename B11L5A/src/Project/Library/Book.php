<?php

namespace App\Project\Library;

class Book {

    public $name = "Math ";
    public $isbn = "1b2ce";
    public $price = "250TK";
    public $pages = 600;
    public $author_name = "Mazhar";

    public function getIsbn() {

        echo 'ISBN number  of this book  is ' . $this->isbn;
    }

    public function getPrice() {

        echo 'Price of this book  is ' . $this->price;
    }

    public function getAuthor_name() {

        echo 'Author  of this book  is ' . $this->author_name;
    }

}
