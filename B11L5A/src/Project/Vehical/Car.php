<?php

namespace App\Project\Vehical;

class Car {

    public $brand = "BMW ";
    public $color = " red";
    public $wheel = "4 ";
    public $body = "wood ";
    public $seat = "4 ";

    public function __construct($color) {
        echo "Color of the car is " . $this->color = $color;
    }

    public function getBrand() {

        echo 'Brand of the car is ' . $this->brand;
    }

    public function getBody() {

        echo 'Body of the car is ' . $this->body;
    }

    public function getSeat() {

        echo 'Seat of the car is ' . $this->seat;
    }

}
