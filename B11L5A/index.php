<?php

include_once 'src/Project/Vehical/Car.php';
include_once 'src/Project/Device/Mobile.php';
include_once 'src/Project/Library/Book.php';
include_once 'src/Project/Animal/Cat.php';

use App\Project\Device\Mobile;
use App\Project\Vehical\Car;
use App\Project\Library\Book;
use App\Project\Animal\Cat;
echo '<b>Car</b>';
echo '<hr>';
$corolla = new Car("blue");
echo '<hr>';
$corolla->getBrand();
echo '<hr>';
$corolla->getBody();
echo '<hr>';
$corolla->getSeat();
echo '<hr>';
echo '<b>Mobile</b>';

echo '<hr>';
$nokia = new Mobile();
echo '<hr>';
$nokia->getBrand();
echo '<hr>';
$nokia->getSize();
echo '<hr>';
$nokia->getColor();
echo '<hr>';
echo '<b>Book</b>';

echo '<hr>';
$new_book = new Book();
echo '<hr>';
$new_book->getIsbn();
echo '<hr>';
$new_book->getPrice();
echo '<hr>';
$new_book->getAuthor_name();
echo '<hr>';
echo '<b>Cat</b>';

echo '<hr>';

$mini = new Cat();
echo '<hr>';
$mini->getName();
echo '<hr>';
$mini->getColor();
echo '<hr>';
$mini->getLegnumber();
