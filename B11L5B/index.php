<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once 'src/Device/Calculator.php';

        use App\Device\Calculator;

$addition = new Calculator(30, 50);
        echo "Addition Result of ".$addition->val1." & ".$addition->val2." is " . $addition->add();
        echo '<hr>';
        $substraction = new Calculator(15, 12);
        echo "Sunstraction Result of ".$substraction->val1." from ".$substraction->val2." is " . $substraction->subtract();
        echo '<hr>';
        $multiplication = new Calculator(20, 2);
        echo "Multiplication Result of ".$multiplication->val1." & ".$multiplication->val2." is " . $multiplication->multiply();
        echo '<hr>';
        $divition = new Calculator(60, 3);
        echo "Divition Result of ".$divition->val1." by ".$divition->val2." is " . $divition->divide();
        echo '<hr>';
        ?>
    </body>
</html>
